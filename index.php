<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $sheep = new animal("shaun");

    echo  "name animal : $sheep->name <br>"; 
    echo  "legs : $sheep->legs <br>"; 
    echo "cold blood : $sheep->cold_blooded <br>";

    $kodok = new frog("buduk");

    echo  "name animal : $kodok->name <br>"; 
    echo  "legs : $kodok->legs <br>"; 
    echo "cold blood : $kodok->cold_blooded <br>";
    echo $kodok->jump();
    echo "<br>";

    $sungokong = new ape("kerasakti");

    echo  "name animal : $sungokong->name <br>"; 
    echo  "legs : $sungokong->legs <br>"; 
    echo "cold blood : $sungokong->cold_blooded <br>";
    echo $sungokong->yell();


?>